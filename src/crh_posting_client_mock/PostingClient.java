package crh_posting_client_mock;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Duration;
import java.util.logging.Logger;


import oracle.jdbc.OracleTypes;


public class PostingClient {
	private final static Logger LOGGER = Logger.getLogger(PostingClient.class.getName());
	
	String dburl = "jldfhasodhaljjsfhjls";
	String username= "sjdhsihdjois";
	String pass = "sudjhsadhoau";
	
	
	
	
	public Result callPostingService(String xml) {
		Result res = new Result();
		String stat ="";
		HttpResponse<String> response;	
		
		try { 
			HttpClient client = HttpClient.newHttpClient();
			HttpRequest request = HttpRequest.newBuilder()
				      .uri(URI.create(postingsrv))
				      .timeout(Duration.ofMinutes(1))
				      .header("Content-Type", "application/xml")
				      .POST(BodyPublishers.ofString(xml))
				      .build();
			
			//client.send(request, BodyHandlers.ofString()).body().toString();
			
			response = client.send(request, BodyHandlers.ofString());  
			stat = response.statusCode() == 200 ? "OK": "FAILED";
			
			LOGGER.info(">>>>>>>>> "+stat);
			
			if (stat.contentEquals("OK")) {
			LOGGER.info("fetching res");
				res.setPxml(response.body().toString());
			}else {
				res.setPxml("failed");
			}
				      
					return res;
					
					
		}catch(Exception e) {
			
			LOGGER.info(e.getMessage());
			res.setResp_msg("99");
			
			res.setPxml("failed to connect to post");
			
		}
		
		return res;
	}
	
	
	
	public Result getxml(String refno, String tokken) {
	Connection conn = null;
	CallableStatement csmt = null;
	Result res = new Result();
	
		try {
	
			conn = DriverManager.getConnection(dburl,username,pass);
			
			if (conn == null) {
				LOGGER.info("failed to obtain connection");
				
				res.setResp_msg("99");
				
				res.setPxml("failed to obtain connection");
				
				return res;
			}
			/// pointing to new pkg
			csmt = conn.prepareCall("{call new_pkg.pr_secure_funds(?,?,?,?,?,?)}");
			csmt.setString(1, refno);
			csmt.setString(2, tokken);
			csmt.registerOutParameter(3, OracleTypes.CLOB);
			csmt.registerOutParameter(4, OracleTypes.VARCHAR);
			csmt.registerOutParameter(5, OracleTypes.VARCHAR);
			csmt.registerOutParameter(6, OracleTypes.VARCHAR);
			
			csmt.execute();
			
			if (csmt.getNString(4).contentEquals("00")) {
				
				res.setResp_msg(csmt.getNString(5));
				
				res.setPxml(csmt.getNString(3));
				
				return res;
				
			}else {
				LOGGER.info("failed on db");
				res.setResp_msg("99");
				
				res.setPxml("failed on db");
				
				return res;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
				try {
					csmt.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}finally {

			try {
				if (csmt != null) {
					csmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		LOGGER.info("failed totally");
		res.setResp_msg("99");
		
		res.setPxml("failed totally");
		
		return res;
	}
	
}

