package crh_posting_client_mock;

public class Result {
	private String resp_msg;
	private String pxml;
	
	public String getResp_msg() {
		return resp_msg;
	}
	public void setResp_msg(String resp_msg) {
		this.resp_msg = resp_msg;
	}
	public String getPxml() {
		return pxml;
	}
	public void setPxml(String pxml) {
		this.pxml = pxml;
	}
	@Override
	public String toString() {
		return "Result [resp_msg=" + resp_msg + ", pxml=" + pxml + "]";
	}
	
	
	
}
